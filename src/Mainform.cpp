#include <QGridLayout>
#include <QLabel>
#include <QStringList>
#include <QApplication>
#include <QProgressDialog>
#include <QMessageBox>
#include <QAction>
#include <QIcon>
#include <QToolBar>
#include <QVariantMap>
#include <QFile>

#include <qmodbus/ModbusEngine.h>
#include <qmodbus/common.h>
#include <settingscmdline/settingscmdline.h>
#include <mylog/mylog.h>
#include <modbus/modbus.h>

#include "DB00.h"
#include "SerialDeviceEnumerator.h"
#include "RecordSetup.h"
#include "QDateTimeDelta.h"

#include "mainform.h"

#ifndef _DEBUG_OUTPUT
#define _DEBUG_OUTPUT   (0)
#endif

Mainform::Mainform(ModbusEngine* mb, QWidget *parent) :
    QMainWindow(parent)
{
    this->mb = mb;
    Sensor = NULL;
    outputFile = NULL;
    mainlayout = new QGridLayout();
    fP = new QLabel(trUtf8("Preassure frequency [Hz]:<center><h1>0.00</h1></center>"));
    fP->setTextFormat(Qt::RichText);
    fT = new QLabel(trUtf8("Temperature frequency [Hz]:<center><h1>0.00</h1></center>"));
    fT->setTextFormat(Qt::RichText);
    P = new QLabel(trUtf8("Preassure [psi]:<center><h1>0.00</h1></center>"));
    P->setTextFormat(Qt::RichText);
    T = new QLabel(trUtf8("Temperature [ᵒC]:<center><h1>0.00</h1></center>"));
    T->setTextFormat(Qt::RichText);
    mainlayout->addWidget(fP, 0, 0);
    mainlayout->addWidget(fT, 0, 1);
    mainlayout->addWidget(P, 1, 0);
    mainlayout->addWidget(T, 1, 1);
    centralWgt = new QWidget;

    RecordAcrion = new QAction(trUtf8("Record to file"), this);
    RecordAcrion->setIcon(QIcon(":/res/media_record.png"));
    RecordAcrion->setCheckable(true);

    QToolBar *toolbar = new QToolBar;
    toolbar->addAction(RecordAcrion);
    addToolBar(Qt::TopToolBarArea, toolbar);

    connect(mb, SIGNAL(ModbusGeneralFailure(int)), this, SLOT(generalModbusfailure()));
    connect(RecordAcrion, SIGNAL(triggered(bool)), this, SLOT(recordToFile(bool)));

    centralWgt->setLayout(mainlayout);
    setCentralWidget(centralWgt);
}

Mainform::~Mainform()
{
    if (RecordAcrion->isChecked())
        RecordAcrion->setChecked(false);
}

void Mainform::DetectDB00()
{
    SerialDeviceEnumerator* enumerator = SerialDeviceEnumerator::instance();
    QStringList avalable = enumerator->devicesAvailable();
#if (_DEBUG_OUTPUT == 1)
    showDeviceInfo(avalable, enumerator);
#endif
    foreach(QString portname, avalable)
    {
        enumerator->setDeviceName(portname);
        if ((enumerator->vendorID() != _FT_VID) || (enumerator->productID() != _FT_PID) || (enumerator->isBusy() == true))
            avalable.removeOne(portname);
        else
            if (portname.at(0) != '/')
            {
                QString number = portname;
                number.remove(0, 3);
                if (number.toInt() > 9)
                {
                    avalable.removeOne(portname);
                    avalable.append("\\\\.\\" + portname);
                }
            }
    }
    enumerator->deleteLater();

    if (avalable.empty())
    {
        QMessageBox::critical(this, trUtf8("No sutable ports found"), trUtf8("No FTDI devises found, make sure the sensor is connected."));
        qApp->exit(2);
    }

    LOG_INFO(trUtf8("Start Scaning ports: %1").arg(avalable.join(", ")));
    QProgressDialog *progress = new QProgressDialog(this);
    connect(progress, SIGNAL(canceled()), this, SLOT(cancelScan()));
    progress->setModal(true);
    progress->setMinimum(1);
    progress->setMaximum(255);
    progress->setValue(1);
    progress->show();

    cancelScanMutex.lock();
    DB00* tempSensor = NULL;
    unsigned char addres;
    struct timeval originalTimeout;
    modbus_get_response_timeout(mb->getModbusContext(), &originalTimeout);

    struct timeval scan_timeout = {0, 40000};
    foreach(QString portname, avalable)
    {
        (*SettingsCmdLine::settings)["Global/Port"] = portname;
        mb->RestartConnection();
        modbus_set_response_timeout(mb->getModbusContext(), &scan_timeout);
        addres = 1;
        while (addres < 255)
        {
            if (cancelScanMutex.tryLock())
            {
                cancelScanMutex.unlock();
                break; // cancel?
            }
            qApp->processEvents();
            progress->setLabelText(trUtf8("Scaning port %1, adress 0x%2").arg(portname).arg(addres, 0, 16));
            progress->setValue(addres);
            tempSensor = new DB00(mb, addres++);
            if (tempSensor->test())
            {
                LOG_INFO(trUtf8("Sensor DB00 found at port %1, adress 0x%2").arg(portname).arg(addres, 0, 16));
                cancelScanMutex.unlock();
                break; //found!
            }
            else
                __DELETE(tempSensor);
        }
        if (cancelScanMutex.tryLock())
        {
            break; // cancel || found
        }
    }

    progress->deleteLater();

    if (tempSensor == NULL)
    {
        QMessageBox::critical(this, trUtf8("No sutable ports found"), trUtf8("Scan complead, no sensors found, make sure the sensor is connected."));
        qApp->exit(1);
    }
    else
    {
        modbus_set_response_timeout(mb->getModbusContext(), &originalTimeout);
        Sensor = tempSensor;
    }
}

void Mainform::updateText(QList<float> values)
{
    if (values.size() != 4)
        return;
    fP->setText(trUtf8("Preassure frequency [Hz]:<center><h1>%1</h1></center>").arg(values.at(2), 0, 'f', 2));
    fT->setText(trUtf8("Temperature frequency [Hz]:<center><h1>%1</h1></center>").arg(values.at(3), 0, 'f', 2));
    P->setText(trUtf8("Preassure [psi]:<center><h1>%1</h1></center>").arg(values.at(0) * kgF2psi, 0, 'f', 2));
    T->setText(trUtf8("Temperature [ᵒC]:<center><h1>%1</h1></center>").arg(values.at(1), 0, 'f', 2));
}

void Mainform::WriteToFile(QList<float> values)
{
    if (outputFile != NULL)
    {
        QString Firstcolumn;
        switch (DateMode)
        {
        case 0:
            Firstcolumn = QDateTime::currentDateTime().toString("yyyy.MM.dd hh:mm:ss.zzz") + ";";
            break;
        case 1:
            Firstcolumn = QDateTimeDelta(QDateTime::currentDateTime(), StartTime) + ";";
            break;
        case 2:
            Firstcolumn = QString::number(counter++) + ";";
            break;
        case 3:
            Firstcolumn = "";
            break;
        }
        QString Measures = WriteTemplate;
        Measures.replace(
                    "%PF%", QString::number(values.at(2), 'f', 2)).replace(
                    "%TF%", QString::number(values.at(3), 'f', 2)).replace(
                    "%P%", QString::number(values.at(0) * kgF2psi, 'f', 2)).replace(
                    "%T%", QString::number(values.at(1), 'f', 2));
#if (_DEBUG_OUTPUT == 1)
        LOG_DEBUG(Firstcolumn + Measures);
#endif
        QByteArray writestring;
        writestring.append(Firstcolumn);
        writestring.append(Measures);
        outputFile->write(writestring);
    }
}

void Mainform::showDeviceInfo(QStringList List, SerialDeviceEnumerator *enumerator)
{
    foreach(QString str, List)
    {
        enumerator->setDeviceName(str);
        LOG_INFO(trUtf8(">>> info about: %1").arg(enumerator->name()));

        LOG_INFO(trUtf8("-> description  : %1").arg(enumerator->description()));
        LOG_INFO(trUtf8("-> driver       : %1").arg(enumerator->driver()));
        LOG_INFO(trUtf8("-> friendlyName : %1").arg(enumerator->friendlyName()));
        LOG_INFO(trUtf8("-> hardwareID   : %1").arg(enumerator->hardwareID().join(" ")));
        LOG_INFO(trUtf8("-> locationInfo : %1").arg(enumerator->locationInfo()));
        LOG_INFO(trUtf8("-> manufacturer : %1").arg(enumerator->manufacturer()));
        LOG_INFO(trUtf8("-> productID    : %1").arg(enumerator->productID()));
        LOG_INFO(trUtf8("-> service      : %1").arg(enumerator->service()));
        LOG_INFO(trUtf8("-> shortName    : %1").arg(enumerator->shortName()));
        LOG_INFO(trUtf8("-> subSystem    : %1").arg(enumerator->subSystem()));
        LOG_INFO(trUtf8("-> systemPath   : %1").arg(enumerator->systemPath()));
        LOG_INFO(trUtf8("-> vendorID     : %1").arg(enumerator->vendorID()));

        LOG_INFO(trUtf8("-> revision     : %1").arg(enumerator->revision()));
        LOG_INFO(trUtf8("-> bus          : %1").arg(enumerator->bus()));
        //
        LOG_INFO(trUtf8("-> is exists    : %1").arg(enumerator->isExists()));
        LOG_INFO(trUtf8("-> is busy      : %1").arg(enumerator->isBusy()));
    }
}

void Mainform::cancelScan()
{
    cancelScanMutex.unlock();
}

void Mainform::generalModbusfailure()
{
    recordToFile(false);
    QMessageBox::critical(this, trUtf8("Connection terminated."), trUtf8("Connection terminated, are you pull cable out?"));
    qApp->exit(3);
}

void Mainform::recordToFile(bool startrecord)
{
    if (startrecord)
    {
        RecordSetup setupwindow;
        setupwindow.exec();
        QVariantMap setup = setupwindow.getSetup();
        if (setup.isEmpty())
        {
            RecordAcrion->setChecked(false);
            return;
        }
#if (_DEBUG_OUTPUT == 1)
        foreach(QString key, setup.keys())
        {
            LOG_INFO(trUtf8("%1 -> %2").arg(key).arg(setup.value(key, "").toString()));
        }
#endif
        outputFile = new QFile(setup.value("filename").toString());
        if (!outputFile->open(QIODevice::WriteOnly | QIODevice::Text))
        {
            LOG_ERROR(trUtf8("Failed to open file %1.").arg(setup.value("filename").toString()));
            __DELETE(outputFile);
            RecordAcrion->setChecked(false);
        }
        else
        {
            DateMode = (char) setup.value("dateMode").toInt();
            QString firstcolumname;
            switch (DateMode)
            {
            case 0:
                firstcolumname = trUtf8("Date;");
                break;
            case 1:
                firstcolumname = trUtf8("Timer;");
                StartTime = QDateTime::currentDateTime();
                break;
            case 2:
                firstcolumname = trUtf8("Counter;");
                counter = 0;
                break;
            }
            QString headText = trUtf8("%1%2%3%4").arg(
                        setup.value("WPF").toBool() ? ("Preassure frequency [Hz];") : ("")).arg(
                        setup.value("WTF").toBool() ? ("Temperature frequency [Hz];") : ("")).arg(
                        setup.value("WP").toBool() ? ("Preassure [psi];") : ("")).arg(
                        setup.value("WT").toBool() ? trUtf8("Temperature [ᵒC];") : (""));
            *(headText.end() - 1) = '\n';
            QByteArray outdata;
            outdata.append(firstcolumname);
            outdata.append(headText);
            outputFile->write(outdata);

            WriteTemplate = trUtf8("%1%2%3%4").arg(
                        setup.value("WPF").toBool() ? ("%PF%;") : ("")).arg(
                        setup.value("WTF").toBool() ? ("%TF%;") : ("")).arg(
                        setup.value("WP").toBool() ? ("%P%;") : ("")).arg(
                        setup.value("WT").toBool() ? ("%T%;") : (""));
            *(WriteTemplate.end() - 1) = '\n';
            RecordAcrion->setIcon(QIcon(":/res/media_stop.png"));
            LOG_INFO(trUtf8("Start recording to file %1").arg(setup.value("filename").toString()));
        }
    }
    else
    {
        if (outputFile != NULL)
        {
            outputFile->close();
            __DELETE(outputFile);
            LOG_INFO(trUtf8("Record stoped"));
        }
        RecordAcrion->setIcon(QIcon(":/res/media_record.png"));
    }
}

void Mainform::timerEvent(QTimerEvent *_e)
{
    if (Sensor == NULL)
        return;
    bool res;
    QList<float> measuredValues = Sensor->getOutputValues(&res);
    if ((!res) || measuredValues.size() != _DB00_DATA_LEN)
        return;
    updateText(measuredValues);
    WriteToFile(measuredValues);
}
