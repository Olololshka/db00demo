#include <stdio.h>

#include <qmodbus/ModbusEngine.h>
#include <qmodbus/common.h>
#include <mylog/mylog.h>

#include "DB00.h"

#define SHOW_SCAN_PROGRESS	(1)

DB00::DB00(ModbusEngine* mb, unsigned char adress, QObject* parent) :
    QObject(parent)
{
    ModBus = mb;
    this->adress = adress;
    getDataRequest01 = ModbusRequest::BuildReadInputsRequest(adress,
                                                           _DB00_OUT_DATA_ADRESS,
                                                             2 * sizeof(float) / sizeof(uint16_t)
                                                           );
    getDataRequest34 = ModbusRequest::BuildReadInputsRequest(adress,
                                                           _DB00_OUT_FREQ_ADRESS,
                                                           2 * sizeof(float) / sizeof(uint16_t)
                                                           );
}

DB00::~DB00()
{
    __DELETE(getDataRequest01);
    __DELETE(getDataRequest34);
}

bool DB00::test()
{
    ModbusRequest* idrequest = ModbusRequest::BuildReadHoldingRequest(adress, _DB00_ID_ADRESS);
    bool res = false;
    ModBus->SyncRequest(*idrequest);
    if (idrequest->getErrorrCode() == ModbusRequest::ERR_OK)
    {
        if (idrequest->getAnsverAsShort().at(0) != _DB00_DEVICE_ID)
            LOG_DEBUG(trUtf8("Found unsupported device ID:%1").arg(idrequest->getAnsverAsShort().at(0), 0, 16));
        else
            res = true;
    }
    __DELETE(idrequest);
    return res;
}

QList<float> DB00::getOutputValues(bool *ok)
{
    ModBus->SyncRequest(*getDataRequest01);
    ModBus->SyncRequest(*getDataRequest34);
    ModbusRequest::ErrorCode err01 = getDataRequest01->getErrorrCode();
    ModbusRequest::ErrorCode err34 = getDataRequest34->getErrorrCode();
    if ((err01 != ModbusRequest::ERR_OK) || (err34 != ModbusRequest::ERR_OK))
    {
        LOG_ERROR(trUtf8("Failed to get data from device!"));
        if (ok != NULL)
            *ok = false;
        return QList<float>();
    }
    else
    {
        if (ok != NULL)
            *ok = true;
        QList<float> res(getDataRequest01->getAnsverAsFloat());
        res.append(getDataRequest34->getAnsverAsFloat());
        return res;
    }
}

unsigned char DB00::getAddres() const
{
    return adress;
}

DB00* DB00::scan(ModbusEngine* mb, unsigned char startAddress)
{
    DB00* testDevice = NULL;
    while (startAddress < 255)
    {
#if (SHOW_SCAN_PROGRESS==1)
        printf("\rScaning address: %02X", startAddress);
        fflush(stdout);
#endif
        testDevice = new DB00(mb, startAddress++);
        if (!testDevice->test())
        {
            __DELETE(testDevice);
        }
        else
            break;
    }
    printf("\n");
    return testDevice;
}
