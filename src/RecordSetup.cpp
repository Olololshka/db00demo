
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QGroupBox>
#include <QRadioButton>
#include <QCheckBox>
#include <QLineEdit>
#include <QFileDialog>
#include <QDir>

#include "RecordSetup.h"

RecordSetup::RecordSetup(QWidget *parent) :
    QDialog(parent)
{
    result = false;
    OkButton = new QPushButton(trUtf8("Ok"));
    CancelButton = new QPushButton(trUtf8("Cancel"));

    QVBoxLayout *mainlayout = new QVBoxLayout;

    QGroupBox *radiogroup = new QGroupBox(trUtf8("Date column"));

    radiogroup_m[0] = new QRadioButton(trUtf8("System date"));
    radiogroup_m[1] = new QRadioButton(trUtf8("Timer"));
    radiogroup_m[2] = new QRadioButton(trUtf8("Simple number"));
    radiogroup_m[3] = new QRadioButton(trUtf8("No date"));
    radiogroup_m[0]->setCheckable(true);
    radiogroup_m[0]->setChecked(true);
    QVBoxLayout *radiolayout = new QVBoxLayout;
    for (int i = 0; i < 4; ++i)
    {
        radiolayout->addWidget(radiogroup_m[i]);
    }
    radiogroup->setLayout(radiolayout);

    mainlayout->addWidget(radiogroup);

    WritePreassureFreq_cb = new QCheckBox(trUtf8("Write frequency of preassure"));
    WriteTemperatureFreq_cb = new QCheckBox(trUtf8("Write frequency of temperature"));
    WritePreassure_cb = new QCheckBox(trUtf8("Write preassure"));
    WriteTemperature_cb = new QCheckBox(trUtf8("Write temperature"));

    WritePreassureFreq_cb->setChecked(true);
    WriteTemperatureFreq_cb->setChecked(true);
    WritePreassure_cb->setChecked(true);
    WriteTemperature_cb->setChecked(true);

    mainlayout->addWidget(WritePreassureFreq_cb);
    mainlayout->addWidget(WriteTemperatureFreq_cb);
    mainlayout->addWidget(WritePreassure_cb);
    mainlayout->addWidget(WriteTemperature_cb);

    QHBoxLayout *filenamelayout = new QHBoxLayout;
    FilenameEdit = new QLineEdit(trUtf8("output.csv"));
    SetFilenameBtn = new QPushButton("...");
    filenamelayout->addWidget(FilenameEdit);
    filenamelayout->addWidget(SetFilenameBtn);
    mainlayout->addLayout(filenamelayout);

    QHBoxLayout *buttonsLayut = new QHBoxLayout;
    buttonsLayut->addWidget(OkButton);
    buttonsLayut->addWidget(CancelButton);
    mainlayout->addLayout(buttonsLayut);

    connect(OkButton, SIGNAL(clicked()), this, SLOT(okSlot()));
    connect(CancelButton, SIGNAL(clicked()), this, SLOT(reject()));
    connect(SetFilenameBtn, SIGNAL(clicked()), this, SLOT(SelectFilenameSlot()));

    setLayout(mainlayout);
}

QVariantMap RecordSetup::getSetup()
{
    if (result)
    {
        QVariantMap result;
        for (int i = 0; i < 4; ++i)
        {
            if (radiogroup_m[i]->isChecked())
            {
                result["dateMode"] = i;
                break;
            }
        }
        result["WPF"] = WritePreassureFreq_cb->isChecked();
        result["WTF"] = WriteTemperatureFreq_cb->isChecked();
        result["WP"] = WritePreassure_cb->isChecked();
        result["WT"] = WriteTemperature_cb->isChecked();
        result["filename"] = FilenameEdit->text();
        return result;
    }
    else
        return QVariantMap();
}

void RecordSetup::okSlot()
{
    result = true;
    accepted();
    close();
}

void RecordSetup::SelectFilenameSlot()
{
    QString res = QFileDialog::getSaveFileName(this,
                                               trUtf8("Save as.."), QDir::currentPath(),
                                               trUtf8("csv (*.csv)"));
    if (!res.isEmpty())
    {
        if (!res.endsWith(".csv", Qt::CaseInsensitive))
            res += ".csv";
        FilenameEdit->setText(res);
    }
}
