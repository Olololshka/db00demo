#ifndef DB00_H_
#define DB00_H_

#include <QObject>
#include <QList>

/// Уникальный идентификатор устройства
#define _DB00_DEVICE_ID          (0xDB00)

/// адрес регистра идентификатора
#define _DB00_ID_ADRESS          (0x0000)

/// начальный регистр блока данных джойстика
#define _DB00_OUT_DATA_ADRESS	(0x0000)

/// адрес начала ячеек с частотой
#define _DB00_OUT_FREQ_ADRESS   (0x0010)

/// Количество данных (float)
#define	_DB00_DATA_LEN			(4)

class ModbusRequest;
class ModbusEngine;

class DB00: public QObject
{
    Q_OBJECT
public:

    DB00(ModbusEngine* mb, unsigned char adress, QObject* parent = NULL);
    virtual ~DB00();

    bool test();
    QList<float> getOutputValues(bool *ok = NULL);
	unsigned char getAddres() const;
	
	static DB00* scan(ModbusEngine* mb, unsigned char startAddress);

private:
    unsigned char adress;
    ModbusEngine* ModBus;
    ModbusRequest *getDataRequest01, *getDataRequest34;
};

#endif /* DB00_H_ */
