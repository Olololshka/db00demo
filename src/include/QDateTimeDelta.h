
#ifndef QDATETIMEOPERATOR__H_
#define QDATETIMEOPERATOR__H_

#include <QDateTime>

QString QDateTimeDelta(const QDateTime & t1, const QDateTime & t2);

#endif /* QDATETIMEOPERATOR__H_ */
