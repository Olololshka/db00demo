#ifndef MAINFORM_H
#define MAINFORM_H

#include <QMainWindow>
#include <QMutex>
#include <QDateTime>

#define kgF2psi     (14.223)

#define _FT_VID     "0403"
#define _FT_PID     "6001"

class QGridLayout;
class QAction;
class DB00;
class QLabel;
class ModbusEngine;
class QFile;

class SerialDeviceEnumerator;

class Mainform : public QMainWindow
{
    Q_OBJECT
public:
    explicit Mainform(ModbusEngine* mb, QWidget *parent = 0);
    virtual ~Mainform();

public slots:
    void DetectDB00();

private:
    QGridLayout* mainlayout;
    QLabel *fP, *fT, *P, *T;
    DB00* Sensor;
    QWidget* centralWgt;
    ModbusEngine* mb;

    QAction* RecordAcrion;

    void updateText(QList<float> values);
    void WriteToFile(QList<float> values);

    static void showDeviceInfo(QStringList List, SerialDeviceEnumerator* enumerator);

    QMutex cancelScanMutex;

    QFile* outputFile;
    QString WriteTemplate;

    char DateMode;
    unsigned int counter;
    QDateTime StartTime;

private slots:
    void cancelScan();
    void generalModbusfailure();
    void recordToFile(bool startrecord);

protected:
    virtual void timerEvent(QTimerEvent* _e);
};

#endif // MAINFORM_H
