#ifndef RECORDSETUP_H
#define RECORDSETUP_H

#include <QDialog>
#include <QVariantMap>

class QPushButton;
class QLineEdit;
class QRadioButton;
class QCheckBox;

class RecordSetup : public QDialog
{
    Q_OBJECT
public:
    explicit RecordSetup(QWidget *parent = 0);

    QVariantMap getSetup();

private:
    bool result;

    QPushButton *OkButton, *CancelButton;

    QLineEdit *FilenameEdit;
    QPushButton* SetFilenameBtn;

    QRadioButton* radiogroup_m[4];

    QCheckBox *WritePreassureFreq_cb;
    QCheckBox *WriteTemperatureFreq_cb;
    QCheckBox *WritePreassure_cb;
    QCheckBox *WriteTemperature_cb;

private slots:
    void okSlot();
    void SelectFilenameSlot();
};

#endif // RECORDSETUP_H
