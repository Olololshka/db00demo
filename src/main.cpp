//-----------------------------------------------------------------
#include <QApplication>
#include <QThread>
#include <QAction>
#include <QString>
#include <QTimer>
//-----------------------------------------------------------------
#include <qmodbus/common.h>
#include <qmodbus/Sleeper.h>
#include <qmodbus/ModbusEngine.h>
#include <qmodbus/ModbusRequest.h>
#include <mylog/mylog.h>
//-----------------------------------------------------------------
#include "Settings.h"
#include "mainform.h"
//-----------------------------------------------------------------

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    app.setApplicationName("JoyMonitor");

    //настройко-держатель
    Settings _settings(argc, argv);

    //Лог
    MyLog::myLog _LOG;

    // синхронно-асинхронный движок Modbus
    ModbusEngine _ModbusCore;

    Mainform mainform(&_ModbusCore); // Главная форма

    mainform.show();

    QTimer::singleShot(100, &mainform, SLOT(DetectDB00()));
    mainform.startTimer(500);

    return app.exec();
}
//-----------------------------------------------------------------
