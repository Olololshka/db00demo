# - Try to find libqmodbus
# Once done this will define
#
#  QQMODBUS_FOUND - system has QMODBUS
#  QQMODBUS_INCLUDE_DIR - the QMODBUS include directory
#  QQMODBUS_LIBRARIES - Link these to use QMODBUS

# Copyright (c) 2006, Jasem Mutlaq <mutlaqja@ikarustech.com>
# Based on FindLibfacile by Carsten Niehaus, <cniehaus@gmx.de>
#
# Redistribution and use is allowed according to the terms of the BSD license.
# For details see the accompanying COPYING-CMAKE-SCRIPTS file.

if (QMODBUS_INCLUDE_DIR AND QMODBUS_LIBRARIES)

  # in cache already
  set(QMODBUS_FOUND TRUE)
  message(STATUS "Found libmodbus: ${QMODBUS_LIBRARIES}")

else (QMODBUS_INCLUDE_DIR AND QMODBUS_LIBRARIES)

  find_path(QMODBUS_INCLUDE_DIR 
	NAMES common.h ModbusEngine.h ModbusRequest.h PerfTimeCounter.h Sleeper.h
    PATH_SUFFIXES qmodbus
	PATHS
		/usr/include
		/usr/local/include
		$ENV{WD}/../include
		$ENV{WD}/../local/include
  )

  find_library(QMODBUS_LIBRARIES NAMES qmodbus
    PATHS
		/usr/lib
		/usr/bin
		/usr/local/lib
		/usr/local/bin
  )

 set(CMAKE_REQUIRED_INCLUDES ${QMODBUS_INCLUDE_DIR})
 set(CMAKE_REQUIRED_LIBRARIES ${QMODBUS_LIBRARIES})

   if(QMODBUS_INCLUDE_DIR AND QMODBUS_LIBRARIES)
    set(QMODBUS_FOUND TRUE)
  else (QMODBUS_INCLUDE_DIR AND QMODBUS_LIBRARIES)
    set(QMODBUS_FOUND FALSE)
  endif(QMODBUS_INCLUDE_DIR AND QMODBUS_LIBRARIES)

  if (QMODBUS_FOUND)
    if (NOT QMODBUS_FIND_QUIETLY)
      message(STATUS "Found libmodbus: ${QMODBUS_LIBRARIES}")
    endif (NOT QMODBUS_FIND_QUIETLY)
  else (QMODBUS_FOUND)
    if (QMODBUS_FIND_REQUIRED)
      message(FATAL_ERROR "libmodbus not found. Please install libmodbus-devel. https://launchpad.net/libmodbus/")
    endif (QMODBUS_FIND_REQUIRED)
  endif (QMODBUS_FOUND)

  mark_as_advanced(QMODBUS_INCLUDE_DIR QMODBUS_LIBRARIES)

endif (QMODBUS_INCLUDE_DIR AND QMODBUS_LIBRARIES)
