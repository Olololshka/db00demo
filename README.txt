Demo programm to show mesured values from sensor QuartzSens DB00

You can resive source code there: https://bitbucket.org/Olololshka/db00demo

Using libraries:
+ Qt >=4.6 (used 4.8.4): http://qt-project.org/search/tag/open~source
+ libqserialdevice: http://gitorious.org/qserialdevice
+ fork of libmodbus: https://github.com/ololoshka2871/libmodbus
+ libqmodbus: https://bitbucket.org/Olololshka/libqmodbus
+ libmylog: https://bitbucket.org/Olololshka/libmylog
+ libsettingscmdline: https://bitbucket.org/Olololshka/libsettingscmdline

+ MinGW for Windows: http://www.mingw.org/

